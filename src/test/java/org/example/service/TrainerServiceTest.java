package org.example.service;

import org.example.dao.TrainerDAO;
import org.example.domain.Trainer;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.*;

public class TrainerServiceTest {

    @Mock
    TrainerDAO trainerDAO;

    private TrainerService trainerService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        trainerService = new TrainerService(trainerDAO);
    }

    @Test
    public void testCreate() {
        Trainer trainer = new Trainer();

        Mockito.when(trainerDAO.createTrainer(Mockito.any(Trainer.class))).thenReturn(trainer);

        Trainer createdTrainer = trainerService.create(trainer);

        assertNotNull(createdTrainer);
    }

    @Test
    public void testUpdate() {
        Trainer trainer = new Trainer();

        Mockito.when(trainerDAO.updateTrainer(Mockito.any(Trainer.class))).thenReturn(trainer);

        Trainer updatedTrainer = trainerService.update(trainer);

        assertNotNull(updatedTrainer);
    }

    @Test
    public void testSelect() {
        Integer trainerId = 1;
        Trainer trainer = new Trainer();
        trainer.setId(trainerId);

        Mockito.when(trainerDAO.getTrainer(Mockito.any(Integer.class))).thenReturn(trainer);

        Trainer selectedTrainer = trainerService.select(trainerId);

        assertNotNull(selectedTrainer);
        assertEquals(trainerId, selectedTrainer.getId());
    }
}
