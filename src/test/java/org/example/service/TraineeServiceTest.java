package org.example.service;

import org.example.dao.TraineeDAO;
import org.example.domain.Trainee;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class TraineeServiceTest {

    @Mock
    TraineeDAO traineeDAO;

    private TraineeService traineeService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        traineeService = new TraineeService(traineeDAO);
    }

    @Test
    public void testCreate() {
        Trainee trainee = new Trainee();

        Mockito.when(traineeDAO.createTrainee(Mockito.any(Trainee.class))).thenReturn(trainee);

        Trainee createdTrainee = traineeService.create(trainee);

        assertNotNull(createdTrainee);
    }

    @Test
    public void testUpdate() {
        Trainee trainee = new Trainee();

        Mockito.when(traineeDAO.updateTrainee(Mockito.any(Trainee.class))).thenReturn(trainee);

        Trainee updatedTrainee = traineeService.update(trainee);

        assertNotNull(updatedTrainee);
    }

    @Test
    public void testDelete() {
        Integer traineeId = 1;

        traineeService.delete(traineeId);

        // Verify that traineeDAO.deleteTrainee was called
        Mockito.verify(traineeDAO, Mockito.times(1)).deleteTrainee(traineeId);
    }

    @Test
    public void testSelect() {
        Integer traineeId = 1;
        Trainee trainee = new Trainee();
        trainee.setId(traineeId);

        Mockito.when(traineeDAO.getTrainee(Mockito.any(Integer.class))).thenReturn(trainee);

        Trainee selectedTrainee = traineeService.select(traineeId);

        assertNotNull(selectedTrainee);
        assertEquals(traineeId, selectedTrainee.getId());
    }
}
