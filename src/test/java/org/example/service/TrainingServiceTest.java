package org.example.service;

import org.example.dao.TrainingDAO;
import org.example.domain.Training;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TrainingServiceTest {
    @Mock
    private TrainingDAO trainingDAO;

    private TrainingService trainingService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this); // Initialize the mocks
        trainingService = new TrainingService(trainingDAO);
    }

    @Test
    public void testCreate() {
        Training training = new Training();

        // Mocking behavior for the DAO
        when(trainingDAO.createTraining(training)).thenReturn(training); // Assuming create is successful

        Training createdTraining = trainingService.create(training);

        // Verify that the createTraining method was called with the correct Training object
        verify(trainingDAO).createTraining(training);

        // Verify that the method returned the created Training
        assertNotNull(createdTraining);
        assertEquals(training, createdTraining);
    }

    @Test
    public void testSelect() {
        int trainingId = 123;
        Training expectedTraining = new Training(); // Create an expected Training object for testing

        // Mocking behavior for the DAO
        when(trainingDAO.getTraining(trainingId)).thenReturn(expectedTraining);

        Training selectedTraining = trainingService.select(trainingId);

        // Verify that the getTraining method was called with the correct trainingId
        verify(trainingDAO).getTraining(trainingId);

        // Verify that the selectedTraining matches the expectedTraining
        assertNotNull(selectedTraining);
        assertEquals(expectedTraining, selectedTraining);
    }
}
