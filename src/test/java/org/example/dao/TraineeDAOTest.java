package org.example.dao;

import org.example.config.InMemoryStorage;
import org.example.domain.Trainee;
import org.example.domain.User;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class TraineeDAOTest {

    @Mock
    InMemoryStorage storage;

    @Mock
    UserDAO userDAO;

    private TraineeDAO traineeDAO;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        traineeDAO = new TraineeDAO(storage, userDAO);
    }

    @Test
    public void testCreateTrainee() {
        Trainee trainee = new Trainee();
        trainee.setUser(new User());

        Mockito.when(storage.getNamespace("Trainee")).thenReturn(new HashMap<>());
        Mockito.when(userDAO.createUser(Mockito.any(User.class))).thenReturn(trainee.getUser());

        Trainee createdTrainee = traineeDAO.createTrainee(trainee);

        assertNotNull(createdTrainee);
        assertEquals(Integer.valueOf(1), createdTrainee.getId());
        assertNotNull(createdTrainee.getUser());
    }

    @Test
    public void testGetTrainee() {
        Map<Integer, Object> trainees = new HashMap<>();
        Trainee trainee = new Trainee();
        trainee.setId(1);
        trainees.put(1, trainee);

        Mockito.when(storage.getNamespace("Trainee")).thenReturn(trainees);

        Trainee foundTrainee = traineeDAO.getTrainee(1);

        assertNotNull(foundTrainee);
        assertEquals(Integer.valueOf(1), foundTrainee.getId());
    }

    @Test
    public void testUpdateTrainee() {
        Map<Integer, Object> trainees = new HashMap<>();
        Trainee trainee = new Trainee();
        trainee.setId(1);
        trainees.put(1, trainee);

        Mockito.when(storage.getNamespace("Trainee")).thenReturn(trainees);
        Mockito.when(userDAO.updateUser(Mockito.any(User.class))).thenReturn(trainee.getUser());

        Trainee updatedTrainee = traineeDAO.updateTrainee(trainee);

        assertNotNull(updatedTrainee);
        assertEquals(Integer.valueOf(1), updatedTrainee.getId());
    }

    @Test
    public void testDeleteTrainee() {
        Map<Integer, Object> trainees = new HashMap<>();
        Trainee trainee = new Trainee();
        trainee.setId(1);
        trainee.setUser(new User());
        trainees.put(1, trainee);

        Mockito.when(storage.getNamespace("Trainee")).thenReturn(trainees);

        traineeDAO.deleteTrainee(1);

        assertNull(trainees.get(1));
    }
}
