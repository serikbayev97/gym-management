package org.example.dao;

import org.example.config.InMemoryStorage;
import org.example.domain.Training;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

public class TrainingDAOTest {

    @Mock
    InMemoryStorage storage;

    private TrainingDAO trainingDAO;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        trainingDAO = new TrainingDAO(storage);
    }

    @Test
    public void testCreateTraining() {
        Training training = new Training();

        when(storage.getNamespace("Training")).thenReturn(new HashMap<>());

        Training createdTraining = trainingDAO.createTraining(training);

        assertNotNull(createdTraining);
        assertEquals(Integer.valueOf(1), createdTraining.getId());
    }

    @Test
    public void testGetTraining() {
        Map<Integer, Object> trainings = new HashMap<>();
        Training training = new Training();
        training.setId(1);
        trainings.put(1, training);

        when(storage.getNamespace("Training")).thenReturn(trainings);

        Training foundTraining = trainingDAO.getTraining(1);

        assertNotNull(foundTraining);
        assertEquals(Integer.valueOf(1), foundTraining.getId());
    }
}
