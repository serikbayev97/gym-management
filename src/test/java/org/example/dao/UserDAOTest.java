package org.example.dao;

import org.example.config.InMemoryStorage;
import org.example.domain.User;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class UserDAOTest {

    @Mock
    InMemoryStorage storage;

    private UserDAO userDAO;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        userDAO = new UserDAO(storage);
    }

    @Test
    public void testCreateUser() {
        User user = new User();
        user.setFirstName("John");
        user.setLastName("Doe");

        Mockito.when(storage.getNamespace("User")).thenReturn(new HashMap<>());

        User createdUser = userDAO.createUser(user);

        assertNotNull(createdUser);
        assertEquals("John.Doe", createdUser.getUsername());
    }

    @Test
    public void testFindUserById() {
        Map<Integer, Object> users = new HashMap<>();
        User user = new User();
        user.setId(1);
        users.put(1, user);

        Mockito.when(storage.getNamespace("User")).thenReturn(users);

        User foundUser = userDAO.findUserById(1);

        assertNotNull(foundUser);
        assertEquals(Integer.valueOf(1), foundUser.getId());
    }

    @Test
    public void testUpdateUser() {
        Map<Integer, Object> users = new HashMap<>();
        User user = new User();
        user.setId(1);
        users.put(1, user);

        Mockito.when(storage.getNamespace("User")).thenReturn(users);

        User updatedUser = new User();
        updatedUser.setId(1);
        updatedUser.setFirstName("UpdatedFirstName");
        updatedUser.setLastName("UpdatedLastName");

        User result = userDAO.updateUser(updatedUser);

        assertNotNull(result);
        assertEquals(Integer.valueOf(1), result.getId());
    }

    @Test
    public void testDeleteUser() {
        Map<Integer, Object> users = new HashMap<>();
        User user = new User();
        user.setId(1);
        users.put(1, user);

        Mockito.when(storage.getNamespace("User")).thenReturn(users);

        userDAO.deleteUser(1);

        assertNull(users.get(1));
    }

    @Test
    public void testIsUsernameTaken() {
        Map<Integer, Object> users = new HashMap<>();
        User user = new User();
        user.setUsername("john.doe");
        users.put(1, user);

        Mockito.when(storage.getNamespace("User")).thenReturn(users);

        assertTrue(userDAO.isUsernameTaken("john.doe"));
        assertFalse(userDAO.isUsernameTaken("jane.doe"));
    }
}
