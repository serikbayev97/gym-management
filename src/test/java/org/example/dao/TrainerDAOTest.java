package org.example.dao;

import org.example.config.InMemoryStorage;
import org.example.domain.Trainer;
import org.example.domain.User;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class TrainerDAOTest {

    @Mock
    InMemoryStorage storage;

    @Mock
    UserDAO userDAO;

    private TrainerDAO trainerDAO;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        trainerDAO = new TrainerDAO(storage, userDAO);
    }

    @Test
    public void testCreateTrainer() {
        Trainer trainer = new Trainer();
        trainer.setUser(new User());

        Mockito.when(storage.getNamespace("Trainer")).thenReturn(new HashMap<>());
        Mockito.when(userDAO.createUser(Mockito.any(User.class))).thenReturn(trainer.getUser());

        Trainer createdTrainer = trainerDAO.createTrainer(trainer);

        assertNotNull(createdTrainer);
        assertEquals(Integer.valueOf(1), createdTrainer.getId());
        assertNotNull(createdTrainer.getUser());
    }

    @Test
    public void testGetTrainer() {
        Map<Integer, Object> trainers = new HashMap<>();
        Trainer trainer = new Trainer();
        trainer.setId(1);
        trainers.put(1, trainer);

        Mockito.when(storage.getNamespace("Trainer")).thenReturn(trainers);

        Trainer foundTrainer = trainerDAO.getTrainer(1);

        assertNotNull(foundTrainer);
        assertEquals(Integer.valueOf(1), foundTrainer.getId());
    }

    @Test
    public void testUpdateTrainer() {
        Map<Integer, Object> trainers = new HashMap<>();
        Trainer trainer = new Trainer();
        trainer.setId(1);
        trainers.put(1, trainer);

        Mockito.when(storage.getNamespace("Trainer")).thenReturn(trainers);
        Mockito.when(userDAO.updateUser(Mockito.any(User.class))).thenReturn(trainer.getUser());

        Trainer updatedTrainer = trainerDAO.updateTrainer(trainer);

        assertNotNull(updatedTrainer);
        assertEquals(Integer.valueOf(1), updatedTrainer.getId());
    }
}
