package org.example.dao;

import org.example.config.InMemoryStorage;
import org.example.domain.Trainee;
import org.example.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Map;
import java.util.logging.Logger;

@Repository
public class TraineeDAO {
    private final InMemoryStorage storage;
    private final UserDAO userDAO;
    private final Logger logger = Logger.getLogger(TraineeDAO.class.getName());

    @Autowired
    public TraineeDAO(InMemoryStorage storage, UserDAO userDAO) {
        this.storage = storage;
        this.userDAO = userDAO;
    }

    public Trainee createTrainee(Trainee trainee) {
        Map<Integer, Object> trainees = storage.getNamespace("Trainee");
        trainee.setId(1);
        trainees.keySet()
                .stream()
                .max(Integer::compareTo)
                .ifPresent(id -> trainee.setId(++id));

        User createdUser = userDAO.createUser(trainee.getUser());
        trainee.setUser(createdUser);
        trainees.put(trainee.getId(), trainee);
        // Log the creation of a trainee
        logger.info("Created Trainee: " + trainee);
        return trainee;
    }

    public Trainee getTrainee(Integer id) {
        Map<Integer, Object> trainees = storage.getNamespace("Trainee");
        Trainee trainee = (Trainee) trainees.get(id);

        // Log the retrieval of a trainee
        if (trainee != null) {
            logger.info("Retrieved Trainee with ID: " + id + ": " + trainee);
        } else {
            logger.warning("No Trainee found with ID: " + id);
        }

        return trainee;
    }

    public Trainee updateTrainee(Trainee trainee) {
        Map<Integer, Object> trainees = storage.getNamespace("Trainee");
        Integer id = trainee.getId();
        if (trainees.containsKey(id)) {
            User updatedUser = userDAO.updateUser(trainee.getUser());
            trainee.setUser(updatedUser);
            trainees.put(id, trainee);

            // Log the update of a trainee
            logger.info("Updated Trainee with ID: " + id + ": " + trainee);
        } else {
            logger.warning("Trainee update failed. No Trainee found with ID: " + id);
        }

        return trainee;
    }

    public void deleteTrainee(Integer id) {
        Map<Integer, Object> trainees = storage.getNamespace("Trainee");
        Trainee trainee = (Trainee) trainees.get(id);
        userDAO.deleteUser(trainee.getUser().getId());
        trainees.remove(id);

        // Log the deletion of a trainee
        logger.info("Deleted Trainee with ID: " + id);
    }
}
