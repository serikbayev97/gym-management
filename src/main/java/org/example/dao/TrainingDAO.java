package org.example.dao;

import org.example.config.InMemoryStorage;
import org.example.domain.Training;
import org.example.domain.TrainingType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Repository
public class TrainingDAO {
    private final InMemoryStorage storage;
    private final Logger logger = Logger.getLogger(TrainingDAO.class.getName());

    @Autowired
    public TrainingDAO(InMemoryStorage storage) {
        this.storage = storage;
    }

    public Training createTraining(Training training) {
        Map<Integer, Object> trainings = storage.getNamespace("Training");
        training.setId(1);
        trainings.keySet().stream()
                .max(Integer::compareTo)
                .ifPresent(id -> training.setId(++id));

        trainings.put(training.getId(), training);

        // Log the creation of a training
        logger.info("Created Training: " + training);
        return training;
    }

    public Training getTraining(Integer id) {
        Map<Integer, Object> trainings = storage.getNamespace("Training");
        Training training = (Training) trainings.get(id);

        // Log the retrieval of a training
        if (training != null) {
            logger.info("Retrieved Training with ID: " + id + ": " + training);
        } else {
            logger.warning("No Training found with ID: " + id);
        }

        return training;
    }

    public List<TrainingType> findAll() {
        return storage.getNamespace("Training")
                .values()
                .stream()
                .map(object -> (TrainingType) object)
                .collect(Collectors.toList());
    }

}
