package org.example.dao;

import org.example.config.InMemoryStorage;
import org.example.domain.Trainer;
import org.example.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Map;
import java.util.Optional;
import java.util.logging.Logger;

@Repository
public class TrainerDAO {
    private final InMemoryStorage storage;
    private final UserDAO userDAO;
    private final Logger logger = Logger.getLogger(TrainerDAO.class.getName());

    @Autowired
    public TrainerDAO(InMemoryStorage storage, UserDAO userDAO) {
        this.storage = storage;
        this.userDAO = userDAO;
    }

    public Trainer createTrainer(Trainer trainer) {
        Map<Integer, Object> trainers = storage.getNamespace("Trainer");
        trainer.setId(1);
        trainers.keySet()
                .stream()
                .max(Integer::compareTo)
                .ifPresent(id -> trainer.setId(++id));

        User createdUser = userDAO.createUser(trainer.getUser());
        trainer.setUser(createdUser);
        trainers.put(trainer.getId(), trainer);

        // Log the creation of a trainer
        logger.info("Created Trainer: " + trainer);
        return trainer;
    }

    public Trainer getTrainer(Integer id) {
        Map<Integer, Object> trainers = storage.getNamespace("Trainer");
        Trainer trainer = (Trainer) trainers.get(id);

        // Log the retrieval of a trainer
        if (trainer != null) {
            logger.info("Retrieved Trainer with ID: " + id + ": " + trainer);
        } else {
            logger.warning("No Trainer found with ID: " + id);
        }

        return trainer;
    }

    public Trainer updateTrainer(Trainer trainer) {
        Map<Integer, Object> trainers = storage.getNamespace("Trainer");
        if (trainers.containsKey(trainer.getId())) {
            User updatedUser = userDAO.updateUser(trainer.getUser());
            trainer.setUser(updatedUser);
            trainers.put(trainer.getId(), trainer);

            // Log the update of a trainer
            logger.info("Updated Trainer with ID: " + trainer.getId() + ": " + trainer);
        } else {
            logger.warning("Trainer update failed. No Trainer found with ID: " + trainer.getId());
        }

        return trainer;
    }
}
