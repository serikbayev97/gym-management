package org.example.dao;

import org.example.config.InMemoryStorage;
import org.example.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.logging.Logger;

@Repository
public class UserDAO {
    private final InMemoryStorage storage;
    private final Logger logger = Logger.getLogger(TraineeDAO.class.getName());

    @Autowired
    public UserDAO(InMemoryStorage storage) {
        this.storage = storage;
    }

    public User createUser(User user) {
        Map<Integer, Object> users = storage.getNamespace("User");
        user.setId(1);
        user.setUsername(generateUsername(user.getFirstName(), user.getLastName()));
        user.setPassword(generateRandomPassword());

        users.keySet()
                .stream()
                .max(Integer::compareTo)
                .ifPresent(id -> user.setId(++id));

        users.put(user.getId(), user);

        logger.info("Created User: " + user);
        return user;
    }

    public User findUserById(Integer userId) {
        Map<Integer, Object> users = storage.getNamespace("User");
        return (User) users.get(userId);
    }

    public User updateUser(User user) {
        Map<Integer, Object> users = storage.getNamespace("User");
        Integer id = user.getId();
        if (users.containsKey(id)) {
            users.put(id, user);

            logger.info("Updated User with ID: " + id + ": " + user);
        } else {
            logger.warning("User update failed. No User found with ID: " + id);
        }

        return user;
    }

    public void deleteUser(Integer id) {
        Map<Integer, Object> users = storage.getNamespace("User");
        users.remove(id);
        logger.info("Deleted User with ID: " + id);
    }

    private String generateUsername(String firstName, String lastName) {
        String baseUsername = firstName + "." + lastName;
        String username = baseUsername;
        int serialNumber = 1;

        // Check if a user with the same username already exists
        while (isUsernameTaken(username)) {
            username = baseUsername + serialNumber;
            serialNumber++;
        }

        return username;
    }

    public boolean isUsernameTaken(String username) {
        boolean isTaken = storage.getNamespace("User")
                .values()
                .stream()
                .map(object -> (User) object)
                .anyMatch(user -> user.getUsername().equals(username));

        // Log the check for username availability
        if (isTaken) {
            logger.info("Username '" + username + "' is taken.");
        } else {
            logger.info("Username '" + username + "' is available.");
        }

        return isTaken;
    }

    public static String generateRandomPassword() {
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        StringBuilder password = new StringBuilder();
        Random random = new Random();
        int passwordLength = 10;

        for (int i = 0; i < passwordLength; i++) {
            int index = random.nextInt(characters.length());
            password.append(characters.charAt(index));
        }

        return password.toString();
    }
}
