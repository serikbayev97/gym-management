package org.example.domain;

import java.util.Objects;

public class Trainer {

    private Integer id;
    private Integer specialization;
    private User user;

    public Trainer() {}

    public Trainer(Integer specialization, User user) {
        this.specialization = specialization;
        this.user = user;
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Integer getSpecialization() {
        return specialization;
    }

    public void setSpecialization(Integer specialization) {
        this.specialization = specialization;
    }

    @Override
    public String toString() {
        return "Trainer{" +
                "id=" + id +
                ", specialization=" + specialization +
                ", user=" + user +
                '}';
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        Trainer trainer = (Trainer) object;
        return Objects.equals(id, trainer.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
