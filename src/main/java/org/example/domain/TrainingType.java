package org.example.domain;

import java.util.Objects;

public class TrainingType {
    private Integer id;
    private String name;

    public TrainingType() {}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        TrainingType that = (TrainingType) object;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    @Override
    public String toString() {
        return "TrainingType{" +
                ", name='" + name + '\'' +
                '}';
    }
}
