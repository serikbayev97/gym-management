package org.example.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:application.properties")
public class ApplicationProperties {

    @Value("${trainers}")
    private String trainersFile;

    @Value("${trainees}")
    private String traineesFile;

    @Value("${trainings}")
    private String trainingsFile;

    @Value("${trainingTypes}")
    private String trainingTypesFile;

    @Value("${users}")
    private String usersFile;

    public String getTrainersFile() {
        return trainersFile;
    }

    public String getTraineesFile() {
        return traineesFile;
    }

    public String getTrainingsFile() {
        return trainingsFile;
    }

    public String getTrainingTypesFile() {
        return trainingTypesFile;
    }
    public String getUsersFile() {
        return usersFile;
    }
}
