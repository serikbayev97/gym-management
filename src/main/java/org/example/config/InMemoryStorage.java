package org.example.config;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class InMemoryStorage {
    private Map<String, Map<Integer, Object>> data = new HashMap<>();

    public void initializeData(Map<String, Map<Integer, Object>> data) {
        this.data = data;
    }

    public Map<Integer, Object> getNamespace(String namespace) {
        return data.get(namespace);
    }

}