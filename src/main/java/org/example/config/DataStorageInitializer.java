package org.example.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.domain.Trainee;
import org.example.domain.Trainer;
import org.example.domain.Training;
import org.example.domain.TrainingType;
import org.example.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Configuration
public class DataStorageInitializer {
    private final ApplicationProperties props;
    private final Logger logger = Logger.getLogger(DataStorageInitializer.class.getName());

    @Autowired
    public DataStorageInitializer(ApplicationProperties props) {
        this.props = props;
    }

    @Bean
    public InMemoryStorage inMemoryStorage() {
        InMemoryStorage storage = new InMemoryStorage();

        List<User> users = mapTo(new ClassPathResource(props.getUsersFile()), User.class);
        List<TrainingType> trainingTypes = mapTo(new ClassPathResource(props.getTrainingTypesFile()), TrainingType.class);
        List<Trainee> trainees = mapTo(new ClassPathResource(props.getTraineesFile()), Trainee.class);
        List<Trainer> trainers = mapTo(new ClassPathResource(props.getTrainersFile()), Trainer.class);
        List<Training> trainings = mapTo(new ClassPathResource(props.getTrainingsFile()), Training.class);

        Map<Integer, Object> usersMap = users.stream().collect(Collectors.toMap(User::getId, Function.identity()));
        Map<Integer, Object> trainingTypeMap = trainingTypes.stream().collect(Collectors.toMap(TrainingType::getId, Function.identity()));
        Map<Integer, Object> traineeMap = trainees.stream().collect(Collectors.toMap(Trainee::getId, Function.identity()));
        Map<Integer, Object> trainerMap = trainers.stream().collect(Collectors.toMap(Trainer::getId, Function.identity()));
        Map<Integer, Object> trainingMap = trainings.stream().collect(Collectors.toMap(Training::getId, Function.identity()));

        Map<String, Map<Integer, Object>> data = new HashMap<>();
        data.put("User", usersMap);
        data.put("TrainingType", trainingTypeMap);
        data.put("Trainee", traineeMap);
        data.put("Trainer", trainerMap);
        data.put("Training", trainingMap);

        storage.initializeData(data);

        // Log the initialization of data
        logger.info("Initialized InMemoryStorage with data.");

        return storage;
    }

    public <T> List<T> mapTo(Resource resource, Class<T> clazz) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(resource.getInputStream(), mapper.getTypeFactory().constructCollectionType(List.class, clazz));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
