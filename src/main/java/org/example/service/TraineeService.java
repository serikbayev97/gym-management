package org.example.service;

import org.example.dao.TraineeDAO;
import org.example.domain.Trainee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.logging.Logger;

@Service
public class TraineeService {
    private final TraineeDAO traineeDAO;
    private final Logger logger = Logger.getLogger(TraineeService.class.getName());

    @Autowired
    public TraineeService(TraineeDAO traineeDAO) {
        this.traineeDAO = traineeDAO;
    }

    public Trainee create(Trainee trainee) {
        logger.info("Creating Trainee: " + trainee);
        return traineeDAO.createTrainee(trainee);
    }

    public Trainee update(Trainee trainee) {
        logger.info("Updating Trainee: " + trainee);
        return traineeDAO.updateTrainee(trainee);
    }

    public void delete(Integer traineeId) {
        logger.info("Deleting Trainee with ID: " + traineeId);
        traineeDAO.deleteTrainee(traineeId);
        logger.info("Trainee deleted with ID: " + traineeId);
    }

    public Trainee select(Integer traineeId) {
        logger.info("Selecting Trainee with ID: " + traineeId);
        Trainee selectedTrainee = traineeDAO.getTrainee(traineeId);

        if (selectedTrainee != null) {
            logger.info("Trainee selected with ID: " + traineeId + ": " + selectedTrainee);
        } else {
            logger.warning("No trainee found with ID: " + traineeId);
        }
        return selectedTrainee;
    }
}
