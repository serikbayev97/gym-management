package org.example.service;

import org.example.dao.TrainingDAO;
import org.example.domain.Training;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TrainingService {
    private final TrainingDAO trainingDAO;

    @Autowired
    public TrainingService(TrainingDAO trainingDAO) {
        this.trainingDAO = trainingDAO;
    }

    public Training create(Training training) {
        return trainingDAO.createTraining(training);
    }

    public Training select(Integer trainingId) {
        return trainingDAO.getTraining(trainingId);
    }

}
