package org.example.service;

import org.example.dao.TrainerDAO;
import org.example.domain.Trainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.logging.Logger;

@Service
public class TrainerService {

    private final Logger logger = Logger.getLogger(TrainerService.class.getName());
    private final TrainerDAO trainerDAO;


    @Autowired
    public TrainerService(TrainerDAO trainerDAO) {
        this.trainerDAO = trainerDAO;
    }

    public Trainer create(Trainer trainer) {
        logger.info("Creating Trainer: " + trainer);
        return trainerDAO.createTrainer(trainer);
    }

    public Trainer update(Trainer trainer) {
        logger.info("Updating Trainer: " + trainer);
        return trainerDAO.updateTrainer(trainer);
    }

    public Trainer select(Integer trainerId) {
        logger.info("Selecting Trainer with ID: " + trainerId);
        Trainer selectedTrainer = trainerDAO.getTrainer(trainerId);

        if (selectedTrainer != null) {
            logger.info("Trainer selected with ID: " + trainerId + ": " + selectedTrainer);
        } else {
            logger.warning("No trainer found with ID: " + trainerId);
        }
        return selectedTrainer;
    }

}
