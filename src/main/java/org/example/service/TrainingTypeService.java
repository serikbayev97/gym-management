package org.example.service;

import org.example.dao.TrainingDAO;
import org.example.domain.TrainingType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TrainingTypeService {
    private final TrainingDAO trainingDAO;

    @Autowired
    public TrainingTypeService(TrainingDAO trainingDAO) {
        this.trainingDAO = trainingDAO;
    }

    public List<TrainingType> getAll() {
        return trainingDAO.findAll();
    }
}
